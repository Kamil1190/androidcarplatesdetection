package com.example.kamil.carplatesdetection;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.kamil.carplatesdetection.models.Character;
import com.example.kamil.carplatesdetection.models.CharacterValueViewModel;
import com.example.kamil.carplatesdetection.models.LearnCharacterAdapter;
import com.example.kamil.carplatesdetection.serialization.Database;
import com.example.kamil.carplatesdetection.webApi.PostInterface;
import com.example.kamil.carplatesdetection.webApi.WebApi;

import java.util.ArrayList;

public class LearnActivity extends AppCompatActivity implements PostInterface{

    Database database;
    WebApi webApi;
    ArrayList<CharacterValueViewModel> characterViewModels;
    ViewHelper viewHelper;
    LearnCharacterAdapter customAdapter;
    boolean isNewActivity = false;
    ProgressBar loadingBar;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(characterViewModels != null && !isNewActivity){
            outState.putParcelableArrayList("key",characterViewModels);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ArrayList<CharacterValueViewModel> savedObject = savedInstanceState.getParcelableArrayList("key");
        if(savedObject != null){
            characterViewModels = savedObject;
            setListView(characterViewModels);
        }
        isNewActivity = false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);

        isNewActivity = false;
        viewHelper = new ViewHelper();
        loadingBar = (ProgressBar) findViewById(R.id.progressBar);
        Intent intent = getIntent();
        Bitmap cameraImage = (Bitmap) intent.getParcelableExtra("BitmapImage");

        //Filtr i segmentacja obrazu
        if(characterViewModels == null){
            ArrayList<Bitmap> characters = viewHelper.imageProcessing(cameraImage);
            boolean close=true;
            if(characters != null){
                if(!characters.isEmpty()){
                    close = false;
                    Toast.makeText(this, "Detected "+characters.size()+" characters.",Toast.LENGTH_SHORT).show();
                    //Niezmienniki
                    characterViewModels = viewHelper.getMomentsInViewModel(characters);
                    //wypisanie w liscie danych
                    setListView(characterViewModels);
                }
            }
            if(close){
                Toast.makeText(this,"Content not found!",Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }

    private void setListView(ArrayList<CharacterValueViewModel> viewModels){
        ListView charactersListView = (ListView) findViewById(R.id.learnListView);
        charactersListView.setItemsCanFocus(true);
        customAdapter = new LearnCharacterAdapter(getApplicationContext(), viewModels);
        charactersListView.setAdapter(customAdapter);
    }

    public void backOnClick(View view){
        finish();
    }

    public void databaseOnClick(View view){
        isNewActivity = true;
        Intent intent = new Intent(this, DatabaseActivity.class);
        startActivity(intent);
    }

    public void saveOnClick(View view){
        if(customAdapter != null){
            characterViewModels = customAdapter.getData();
            ArrayList<CharacterValueViewModel> actualData = new ArrayList<>();
            String values = "";
            for(CharacterValueViewModel object : characterViewModels){
                if(!object.getDecision().isEmpty()){
                    actualData.add(object);
                    values += object.getDecision()+",";
                }
            }

            if(!actualData.isEmpty()){
                Toast.makeText(this, "Saving: "+values+" count: "+actualData.size(),Toast.LENGTH_SHORT).show();
                ArrayList<Character> charactersToSave = new ArrayList<>();
                for(CharacterValueViewModel object : actualData){
                    charactersToSave.add(new Character(object));
                }
                webApi = new WebApi();
                webApi.post(charactersToSave,this).execute();
                loadingBar.setVisibility(View.VISIBLE);
            } else {
                Toast.makeText(this, "Enter character to save",Toast.LENGTH_SHORT).show();
            }

        }
    }


    @Override
    public void processFinish(boolean isComplete, ArrayList<Character> sendObjects) {
        if(!isComplete){
            Toast.makeText(this, "Saved local Database.",Toast.LENGTH_SHORT).show();
            database = new Database(this);
            database.saveData(sendObjects);
        } else {
            Toast.makeText(this, "Saved WebAPI.",Toast.LENGTH_SHORT).show();
        }
        loadingBar.setVisibility(View.INVISIBLE);
    }
}
