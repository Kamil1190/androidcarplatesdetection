package com.example.kamil.carplatesdetection.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;

/**
 * Created by Kamil on 2016-12-02.
 */

public class CharacterValueViewModel extends Character implements Parcelable{

    protected Bitmap characterBitmap;

    public CharacterValueViewModel(Bitmap characterBitmap, Double invariantOne, Double invariantTwo,Double invariantThree,Double invariantFour, String decision) {
        super(invariantOne, invariantTwo,invariantThree,invariantFour, decision);
        this.characterBitmap = characterBitmap;
    }


    public Character getCharacter(){
        return new Character(getInvariantOne(), getInvariantTwo(),getInvariantThree(),getInvariantFour(), getDecision());
    }

    public void setCharacter(Character character){
        setInvariantOne(character.getInvariantOne());
        setInvariantTwo(character.getInvariantTwo());
        setInvariantThree(character.getInvariantThree());
        setInvariantFour(character.getInvariantFour());
        setDecision(character.getDecision());
    }


    public Bitmap getCharacterBitmap() {
        return characterBitmap;
    }

    public void setCharacterBitmap(Bitmap characterBitmap) {
        this.characterBitmap = characterBitmap;
    }


    //Parcel

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //bitmapa
        characterBitmap.writeToParcel(dest, 0);
        dest.setDataPosition(0);

        //reszta
        dest.writeDouble(invariantOne);
        dest.writeDouble(invariantTwo);
        dest.writeDouble(invariantThree);
        dest.writeDouble(invariantFour);
        dest.writeString(decision);
    }

    protected CharacterValueViewModel(Parcel in) {
        super(in.readDouble(),in.readDouble(),in.readDouble(),in.readDouble(), in.readString());
        characterBitmap = Bitmap.CREATOR.createFromParcel(in);
        //characterBitmap = in.readParcelable(Bitmap.class.getClassLoader());
        decision = in.readString();
    }

    public static final Creator<CharacterValueViewModel> CREATOR = new Creator<CharacterValueViewModel>() {
        @Override
        public CharacterValueViewModel createFromParcel(Parcel in) {
            return new CharacterValueViewModel(in);
        }

        @Override
        public CharacterValueViewModel[] newArray(int size) {
            return new CharacterValueViewModel[size];
        }
    };

}
