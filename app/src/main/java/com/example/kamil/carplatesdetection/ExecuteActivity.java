package com.example.kamil.carplatesdetection;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.kamil.carplatesdetection.models.Character;
import com.example.kamil.carplatesdetection.models.CharacterAdapter;
import com.example.kamil.carplatesdetection.models.CharacterValueViewModel;
import com.example.kamil.carplatesdetection.serialization.Database;
import com.example.kamil.carplatesdetection.webApi.DownloadInterface;
import com.example.kamil.carplatesdetection.webApi.WebApi;

import java.util.ArrayList;

public class ExecuteActivity extends AppCompatActivity implements DownloadInterface {

    private ArrayList<CharacterValueViewModel> characterViewModels;
    private ViewHelper viewHelper;
    private Database database;
    private WebApi webApi;
    private ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_execute);

        Intent intent = getIntent();
        Bitmap cameraImage = (Bitmap) intent.getParcelableExtra("BitmapImage");
        viewHelper = new ViewHelper();
        loadingBar = (ProgressBar) findViewById(R.id.progressBar);

        //Filtr i segmentacja obrazu
        ArrayList<Bitmap> characters = viewHelper.imageProcessing(cameraImage);
        boolean close=true;
        if(characters != null){
            if(!characters.isEmpty()){
                close = false;
                //Niezmienniki
                characterViewModels = viewHelper.getMomentsInViewModel(characters);
                //Pobranie danych przez metode asynchroniczna
                webApi = new WebApi();
                webApi.download(this).execute();
                //dalej w processFinish
            }
        }
        if(close){
            Toast.makeText(this,"Content not found!",Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void backToMain(View view)
    {
        finish();
    }


    private void setListView(ArrayList<CharacterValueViewModel> viewModels){
        ListView charactersListView = (ListView) findViewById(R.id.charactersListView);
        CharacterAdapter customAdapter = new CharacterAdapter(getApplicationContext(), viewModels);
        charactersListView.setAdapter(customAdapter);
    }



    @Override
    public void processStart() {
        loadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void processFinish(ArrayList<Character> values) {
        boolean isWebAPI = true;
        if(values==null){   //webapi nic nie zwrocilo to pobieramy lokalnie
            isWebAPI = false;
            values = (new Database(this)).loadData();
        }
        //schowanie ikony ładowania
        loadingBar.setVisibility(View.INVISIBLE);

        //Nadanie decyzji
        characterViewModels = viewHelper.getDecisionInViewModels(characterViewModels, values);
        //wypisanie w liscie danych
        setListView(characterViewModels);
        if(isWebAPI){
            Toast.makeText(this, "Detected "+characterViewModels.size()+" characters, from WebAPI",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Detected "+characterViewModels.size()+" characters. from local Database",Toast.LENGTH_SHORT).show();
        }

    }
}
