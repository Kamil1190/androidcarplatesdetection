package com.example.kamil.carplatesdetection.webApi;

import com.example.kamil.carplatesdetection.models.Character;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-12-13.
 */

public interface PostInterface {
    void processFinish(boolean isComplete, ArrayList<Character> sendObjects);
}
