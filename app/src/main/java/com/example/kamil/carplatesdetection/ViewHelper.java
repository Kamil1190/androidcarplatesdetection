package com.example.kamil.carplatesdetection;

import android.graphics.Bitmap;

import com.example.kamil.carplatesdetection.dataMining.Knn;
import com.example.kamil.carplatesdetection.imageProcessing.ImageProcessing;
import com.example.kamil.carplatesdetection.imageProcessing.Moments;
import com.example.kamil.carplatesdetection.models.Character;
import com.example.kamil.carplatesdetection.models.CharacterValueViewModel;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-11-29.
 */

public class ViewHelper {

    ImageProcessing imageProcessing;

    public ViewHelper(){
        imageProcessing = new ImageProcessing();
    }


    public ArrayList<CharacterValueViewModel> getMomentsInViewModel(ArrayList<Bitmap> bitmapCharacters){

        ArrayList<CharacterValueViewModel> characters = new ArrayList<CharacterValueViewModel>();
        for(int i=0;i<bitmapCharacters.size();i++){
            Bitmap character = bitmapCharacters.get(i);

            Moments moments = new Moments(character);
            Double momentOne = moments.invariantMomentOne();
            Double momentTwo = moments.invariantMomentTwo();
            Double momentThree = moments.invariantMomentThree();
            Double momentFour = moments.invariantMomentFour();

            characters.add(new CharacterValueViewModel(character,momentOne,momentTwo,momentThree,momentFour,""));
        }
        return characters;
    }

    public ArrayList<CharacterValueViewModel> getDecisionInViewModels (ArrayList<CharacterValueViewModel> characterValueViewModels, ArrayList<Character> data){
        Knn knn = new Knn(data,3);
        for(int i=0;i<characterValueViewModels.size();i++){
            CharacterValueViewModel actual = characterValueViewModels.get(i);
            String decision = knn.getDecision(actual.getCharacter());
            actual.setDecision(decision);
            characterValueViewModels.set(i, actual);
        }
        return  characterValueViewModels;
    }

    public ArrayList<Bitmap> imageProcessing(Bitmap cameraImage){
        cameraImage = imageProcessing.filterBlackWhite(cameraImage);
        Bitmap contentPlate = imageProcessing.getContentPlate(cameraImage);
        if(contentPlate != null){
            ArrayList<Bitmap> bitmapCharacters = imageProcessing.segmentationCharacters(contentPlate);
            return bitmapCharacters;
        }
        return null;
    }
}
