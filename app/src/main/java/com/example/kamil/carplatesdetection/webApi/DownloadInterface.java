package com.example.kamil.carplatesdetection.webApi;

import com.example.kamil.carplatesdetection.models.Character;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-12-13.
 */

public interface DownloadInterface {
    void processStart();
    void processFinish(ArrayList<Character> values);
}
