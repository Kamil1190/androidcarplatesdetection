package com.example.kamil.carplatesdetection.webApi;

import android.os.AsyncTask;
import android.util.Log;

import com.example.kamil.carplatesdetection.models.Character;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kamil on 2016-12-13.
 */

public class WebApi {

    private Download download;
    private Post post;

    public Download download(DownloadInterface response) {
        return new Download(response);
    }

    public Post post(ArrayList<Character> object, PostInterface response) {
        return new Post(object, response);
    }




    public class Download extends AsyncTask<Void, Void, ArrayList<Character>>{

        DownloadInterface delegate = null;

        public Download(DownloadInterface delegate) {
            this.delegate = delegate;
        }

        @Override
        protected void onPreExecute() {
            delegate.processStart();
        }

        @Override
        protected ArrayList<Character> doInBackground(Void... params) {

            //test
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } finally {
//            return "Complete!";
//        }

            try {
                URL url = new URL("http://carplatesdetection.apphb.com/api/Characters");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    //pobieranie z WebAPI
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);

                    }
                    bufferedReader.close();

                    //parsowanie JSON
                    ArrayList<Character> characters = new ArrayList<Character>();
                    JSONArray jsonarray = new JSONArray(stringBuilder.toString());
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        double invariantOne = jsonobject.getDouble("invariantOne");
                        double invariantTwo = jsonobject.getDouble("invariantTwo");
                        double invariantThree = jsonobject.getDouble("invariantThree");
                        double invariantFour = jsonobject.getDouble("invariantFour");
                        String decision = jsonobject.getString("decision");
                        characters.add(new Character(invariantOne,invariantTwo,invariantThree,invariantFour,decision));
                    }
                    return characters;
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally{
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Character> characters) {
            delegate.processFinish(characters);
        }

    }








    public class Post extends AsyncTask<Void, Void, Boolean>{

        ArrayList<Character> characters;
        PostInterface delegate;

        public Post(ArrayList<Character> characters, PostInterface delegate) {
            this.characters = characters;
            this.delegate = delegate;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                URL url = new URL("http://carplatesdetection.apphb.com/api/Characters"); //Enter URL here
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod("POST"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
                httpURLConnection.setRequestProperty("Content-Type", "application/json"); // here you are setting the `Content-Type` for the data you are sending which is `application/json`
                httpURLConnection.connect();
                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());

                //single object
//                Character actual = characters.get(0);
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("id",0);
//                jsonObject.put("invariantOne",actual.getInvariantOne());
//                jsonObject.put("invariantTwo",actual.getInvariantTwo());
//                jsonObject.put("invariantThree",actual.getInvariantThree());
//                jsonObject.put("invariantFour",actual.getInvariantFour());
//                jsonObject.put("decision",actual.getDecision());
//                Log.d("OutPut JSON:", jsonObject.toString());
//                wr.writeBytes(jsonObject.toString());

                //array objects
                String toSendJson = createJsonArray(characters);
                Log.d("OutPut array JSON:", toSendJson);
                wr.writeBytes(toSendJson);

                int responseCode=httpURLConnection.getResponseCode();
                String response="";
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                }
                Log.d("WebApi response", response +"\n raw:"+responseCode);

                wr.flush();
                wr.close();

                return true;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            delegate.processFinish(aBoolean, characters);
        }

        public String createJsonArray(ArrayList<Character> characters){
            JSONArray jsonArray = new JSONArray();
            for(Character actual : characters) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", 0);
                    jsonObject.put("invariantOne", actual.getInvariantOne());
                    jsonObject.put("invariantTwo", actual.getInvariantTwo());
                    jsonObject.put("invariantThree", actual.getInvariantThree());
                    jsonObject.put("invariantFour", actual.getInvariantFour());
                    jsonObject.put("decision", actual.getDecision());

                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return jsonArray.toString();
        }

    }
}
