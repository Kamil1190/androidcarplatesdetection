package com.example.kamil.carplatesdetection.serialization;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.kamil.carplatesdetection.models.Character;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kamil on 2016-11-28.
 */

public class Database {

    Context context;

    public Database(Context context){
        this.context = context;
    }


    private void save(ArrayList<Character> characters){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            fos = context.openFileOutput("charactersData.ser", Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(characters);
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Error load FileNotFoundException", Toast.LENGTH_SHORT).show();
            Log.d("Error", e.getMessage());
        } catch (IOException e) {
            Toast.makeText(context, "Error load serialize and save", Toast.LENGTH_SHORT).show();
            Log.d("Error", e.getMessage());
        } finally {
            // zamykamy strumienie w finally
            try {
                if (oos != null) oos.close();
            } catch (IOException e) {}
            try {
                if (fos != null) fos.close();
            } catch (IOException e) {}
        }

    }

    public ArrayList<Character> loadData(){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        ArrayList<Character> characters = null;
        try {
            fis = context.openFileInput("charactersData.ser");
            //fis = new FileInputStream("charactersData.ser"); //utworzenie strumienia wejściowego
            ois = new ObjectInputStream(fis); //utworzenie obiektu odczytującego obiekty ze strumienia

            characters = (ArrayList<Character>) ois.readObject(); //deserializacja obiektu

        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Error load FileNotFoundException", Toast.LENGTH_SHORT).show();
            Log.d("Error load", e.getMessage());
        } catch (IOException e) {
            Toast.makeText(context, "Error load serialize and save", Toast.LENGTH_SHORT).show();
            Log.d("Error load", e.getMessage());
        } catch (ClassNotFoundException e) {
            Toast.makeText(context, "Error load ClassNotFoundException", Toast.LENGTH_SHORT).show();
            Log.d("Error load", e.getMessage());
        } finally {
            // zasoby zwalniamy w finally
            try {
                if (ois != null) ois.close();
            } catch (IOException e) {}
            try {
                if (fis != null) fis.close();
            } catch (IOException e) {}
        }
        return characters;
    }

    public void saveData(ArrayList<Character> characters){
        ArrayList<Character> data = loadData();

        if(data != null){
            data.addAll(characters);
        } else {
            data = characters;
        }
        //usuniecie powtarzajacych sie
        data = new ArrayList(new HashSet(data));

        save(data);
    }
}
