package com.example.kamil.carplatesdetection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.kamil.carplatesdetection.models.Character;
import com.example.kamil.carplatesdetection.models.DatabaseCharacterAdaper;
import com.example.kamil.carplatesdetection.serialization.Database;
import com.example.kamil.carplatesdetection.webApi.DownloadInterface;
import com.example.kamil.carplatesdetection.webApi.WebApi;

import java.util.ArrayList;

public class DatabaseActivity extends AppCompatActivity implements DownloadInterface {

    Database database;
    WebApi webApi;
    ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        loadingBar = (ProgressBar) findViewById(R.id.progressBar);

        webApi = new WebApi();
        webApi.download(this).execute();

    }


    private void setListView(ArrayList<Character> characters){
        ListView characterListView = (ListView) findViewById(R.id.databaseListView);
        DatabaseCharacterAdaper adapter = new DatabaseCharacterAdaper(getApplicationContext(), characters);
        characterListView.setAdapter(adapter);
    }

    @Override
    public void processStart() {
        //change ui
        loadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void processFinish(ArrayList<Character> characters) {
        loadingBar.setVisibility(View.INVISIBLE);
        //check downloading error
        if(characters != null){
            Toast.makeText(this, "WebAPI:"+characters.size()+" items",Toast.LENGTH_SHORT).show();

        } else {    //elece use local data
            database = new Database(this);
            characters = database.loadData();
            if(characters != null){
                Toast.makeText(this,"Local Database: "+characters.size()+" items",Toast.LENGTH_SHORT).show();
            }
        }
        //show data
        setListView(characters);
    }
}
