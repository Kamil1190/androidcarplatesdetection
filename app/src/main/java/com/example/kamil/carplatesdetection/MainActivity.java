package com.example.kamil.carplatesdetection;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.kamil.carplatesdetection.imageProcessing.ImageProcessing;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class MainActivity extends AppCompatActivity {

    private File imageFile;
    public static final int CAMERA_REQUEST = 10;
    private ImageView imgPhoto;
    private Bitmap cameraImage;
    private byte[] imageByteArray;
    ImageProcessing imageProcessing;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
        imageProcessing = new ImageProcessing();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(imageByteArray != null){
            outState.putByteArray("bitmapArray", imageByteArray);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        imageByteArray = savedInstanceState.getByteArray("bitmapArray");
        if(imageByteArray != null){
            cameraImage = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);
            showPhoto();
        }

    }

    public void executePhoto(View view)
    {
        if(cameraImage != null){
            Intent intent = new Intent(this, ExecuteActivity.class);
            intent.putExtra("BitmapImage",cameraImage);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Take foto first", Toast.LENGTH_SHORT).show();
        }

    }

    public void learnOnClick(View view){
        if(cameraImage != null){
            Intent intent = new Intent(this, LearnActivity.class);
            intent.putExtra("BitmapImage",cameraImage);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Take foto first", Toast.LENGTH_SHORT).show();
        }
    }

    public void exitApp(View view)
    {
        finish();
    }

    public void clearPhoto(View view){
        imgPhoto.setImageResource(R.mipmap.camera_icon_min);
        android.view.ViewGroup.LayoutParams layoutParams = imgPhoto.getLayoutParams();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        imgPhoto.setLayoutParams(layoutParams);
        imageByteArray = null;
        cameraImage = null;
    }

    public void databaseOnClick(View view){
        Intent intent = new Intent(this, DatabaseActivity.class);
        startActivity(intent);
    }

    public void getPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void showPhoto(){
        if(cameraImage != null){
            android.view.ViewGroup.LayoutParams layoutParams = imgPhoto.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            imgPhoto.setLayoutParams(layoutParams);

            imgPhoto.setImageBitmap(cameraImage);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            cameraImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
            imageByteArray = stream.toByteArray();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == CAMERA_REQUEST && data != null) {
            cameraImage = (Bitmap) data.getExtras().get("data");
            showPhoto();
        }
    }
}

