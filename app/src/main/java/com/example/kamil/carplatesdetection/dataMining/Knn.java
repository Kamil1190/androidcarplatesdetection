package com.example.kamil.carplatesdetection.dataMining;

import android.util.Log;

import com.example.kamil.carplatesdetection.models.Character;
import com.example.kamil.carplatesdetection.models.CharacterValueViewModel;

import java.util.ArrayList;

/**
 * Created by Damian on 2016-12-03.
 */

public class Knn {

    ArrayList<Character> data;
    int n;

    public Knn(ArrayList<Character> data, int n) {
        this.data = data;
        this.n = n;
    }

    //Lista przechowywująca tylko n najblizszych
    private class NearstCharacters extends ArrayList<CharacterDistanceHolder>{

        @Override
        public boolean add(CharacterDistanceHolder newCharacterDistanceHolder) {
            if(size()<n){
                return super.add(newCharacterDistanceHolder);
            }
            //sprawdzenie czy dystans jest mniejszy od najwiekszego w tym zbiorze
            else if(getBiggestCharacterDistance().distance > newCharacterDistanceHolder.distance){
                int replaceIndex = indexOf(getBiggestCharacterDistance());
                if(replaceIndex!=-1){
                    set(replaceIndex, newCharacterDistanceHolder);
                }
            }
            return false;
        }

        //znalezienie obiektu który ma najwiekszy dystans
        private CharacterDistanceHolder getBiggestCharacterDistance(){
            CharacterDistanceHolder maxDistanceCharacter = get(0);
            for(int i=1;i<size();i++){
                if(get(i).distance>maxDistanceCharacter.distance){
                    maxDistanceCharacter = get(i);
                }
            }
            return maxDistanceCharacter;
        }

        //wydobycie listy znaków
        public ArrayList<Character> getArrayListCharacters(){
            ArrayList<Character> list = new ArrayList<>();
            for(CharacterDistanceHolder actual : this ){
                list.add(actual.character);
            }
            return list;
        }
    }

    //klasa wewnetrzna opisujaca znak wraz z jego odlegloscia
    private class CharacterDistanceHolder{
        Character character;
        double distance;

        public CharacterDistanceHolder(Character character, double distance) {
            this.character = character;
            this.distance = distance;
        }
    }


    private ArrayList<Character> findNNearst(Character object){
        double nearstDistance = -1;
        NearstCharacters nearst = new NearstCharacters();

        for (int i=0;i<data.size();i++){
            Character actualToCompare = data.get(i);

            //metoda mierzenia odległości
            double distance = getMyDistance(actualToCompare, object);
            //double distance = getManhattanDistance(actualToCompare, object);
            //ta lista doda sobie tylko n najblizszych
            nearst.add(new CharacterDistanceHolder(actualToCompare, distance));
        }
        return nearst.getArrayListCharacters();
    }


    private double getManhattanDistance(Character obj1, Character obj2){
        return (Math.abs(obj1.getInvariantOne() - obj2.getInvariantOne())) +
                Math.abs(obj1.getInvariantTwo() - obj2.getInvariantTwo()) +
                Math.abs(obj1.getInvariantThree() - obj2.getInvariantThree()) +
                Math.abs(obj1.getInvariantFour() - obj2.getInvariantFour());
    }

    //wyznaczenie proporcjonalnego ilorazu różnicy wartosci do sumy wartości miedzy niezmiennikami obu obiektów i wyznaczenie odchylenia standardowego
    private double getMyDistance(Character obj1, Character obj2){
        int n=4; //nr Invariant
        double[] fInvariant = new double[n];
        for(int i=1;i<4;i++){
            //dla kazdego niezmiennika liczona jest różnica proporcjonalna
            fInvariant[i] = ((Math.abs(obj1.getInvariant(i) - obj2.getInvariant(i))) / (Math.abs(obj1.getInvariant(i) + obj2.getInvariant(i))));
        }
        //wyznaczenie odchylenia standardowego
        Statistics stat = new Statistics(fInvariant);
        return stat.getStdDev();
    }



    public String getDecision(Character object){
        if(object != null){
            ArrayList<Character> nearst = findNNearst(object);
            ArrayList<String> decision = new ArrayList<>();
            ArrayList<Integer> countDecision = new ArrayList<>();
            //stworzenie listy najbardziej powtarzalnych decyzji dla najblizszych sąsiadów
            for(int i=0;i<nearst.size();i++){
                Character actualNearst = nearst.get(i);
                int indexDecision = decision.indexOf(actualNearst.getDecision());
                //jesli jest juz dana decyzja w zbiorze to zwiekszamy jej wystapienie
                if(indexDecision != -1){
                    countDecision.set(indexDecision, countDecision.get(indexDecision)+1);
                }
                //jesli nie ma to dodajemy do zbioru z wystapieniem 1
                else {
                    decision.add(actualNearst.getDecision());
                    countDecision.add(1);
                }
            }
            //wybranie tej najczęstrzej
            int indexMostCommon = 0;
            for(int i=1;i<countDecision.size();i++){
                if(countDecision.get(indexMostCommon)<countDecision.get(i)){
                    indexMostCommon=i;
                }
            }
            return decision.get(indexMostCommon);
        }
        return "";
    }

}
