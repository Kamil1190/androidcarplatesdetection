package com.example.kamil.carplatesdetection.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kamil.carplatesdetection.R;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-11-28.
 */

public class CharacterAdapter extends ArrayAdapter<CharacterValueViewModel> {


    public CharacterAdapter(Context context, ArrayList<CharacterValueViewModel> characters) {
        super(context, 0, characters);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View result = convertView;

        if(result == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            result = inflater.inflate(R.layout.row_item, parent, false);

        }

        CharacterValueViewModel data = getItem(position);

        final TextView decisionTextView = (TextView) result.findViewById(R.id.decisionTextView);
        final ImageView characterImageView = (ImageView) result.findViewById(R.id.characterImageView);

        decisionTextView.setText(data.getDecision());
        characterImageView.setImageBitmap(data.getCharacterBitmap());

        return result;
    }
}