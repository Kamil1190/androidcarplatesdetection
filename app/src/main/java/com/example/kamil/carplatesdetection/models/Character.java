package com.example.kamil.carplatesdetection.models;

import java.io.Serializable;

/**
 * Created by Kamil on 2016-11-29.
 */

public class Character implements Serializable {

    protected Double invariantOne;
    protected Double invariantTwo;
    protected Double invariantThree;
    protected Double invariantFour;
    protected String decision;

    public Character(Double invariantOne, Double invariantTwo,Double invariantThree,Double invariantFour, String decision) {
        this.invariantOne = invariantOne;
        this.invariantTwo = invariantTwo;
        this.invariantThree = invariantThree;
        this.invariantFour = invariantFour;
        this.decision = decision;
    }

    public Character(CharacterValueViewModel viewModel){
        this.invariantOne = viewModel.getInvariantOne();
        this.invariantTwo = viewModel.getInvariantTwo();
        this.invariantThree = viewModel.getInvariantThree();
        this.invariantFour = viewModel.getInvariantFour();
        this.decision = viewModel.getDecision();
    }

    public Double getInvariant(int index){
        switch (index){
            case 1: return getInvariantOne();
            case 2: return getInvariantTwo();
            case 3: return getInvariantThree();
            case 4: return getInvariantFour();
        }
        return null;
    }

    public Double getInvariantOne() {
        return invariantOne;
    }

    public void setInvariantOne(Double invariantOne) {

        this.invariantOne = invariantOne;
    }

    public Double getInvariantTwo() {
        return invariantTwo;
    }


    public void setInvariantTwo(Double invariantTwo) {
        this.invariantTwo = invariantTwo;
    }

    public Double getInvariantThree() {
        return invariantThree;
    }

    public void setInvariantThree(Double invariantThree) {
        this.invariantThree = invariantThree;
    }

    public Double getInvariantFour() {
        return invariantFour;
    }

    public void setInvariantFour(Double invariantFour) {
        this.invariantFour = invariantFour;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Character character = (Character) o;

        if (!invariantOne.equals(character.invariantOne)) return false;
        if (!invariantTwo.equals(character.invariantTwo)) return false;
        if (!invariantThree.equals(character.invariantThree)) return false;
        if (!invariantFour.equals(character.invariantFour)) return false;
        return decision.equals(character.decision);

    }

    @Override
    public int hashCode() {
        int result = invariantOne.hashCode();
        result = 31 * result + invariantTwo.hashCode();
        result = 31 * result + decision.hashCode();
        return result;
    }
}
