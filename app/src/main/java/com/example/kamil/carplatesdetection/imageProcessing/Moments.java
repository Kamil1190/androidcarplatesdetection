package com.example.kamil.carplatesdetection.imageProcessing;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by Kamil on 2016-11-21.
 */

public class Moments {

    private Bitmap bitmap;

    public Moments(Bitmap bitmap){
        this.bitmap = bitmap;
    }

    private int getMoment(int p, int q){
        int suma = 0;
        for(int i=1;i<bitmap.getWidth();i++){
            for(int j=1;j<bitmap.getHeight();j++){
                suma += Math.pow(i,p)*Math.pow(j,p)*getPixel(i,j);
            }
        }
        return suma;
    }

    private int getPixel(int x, int y){
        int valuePixel = bitmap.getPixel(x,y);
        int red = Color.red(valuePixel);
        int blue = Color.blue(valuePixel);
        int green = Color.green(valuePixel);
        if(red == 255 && blue == 255 && green == 255){
            return 0;
        } else if(red == 0 && blue == 0 && green == 0){
            return 1;
        }
        return 0;
    }

    private double twoZeroNormalInvariant(){
        return getMoment(2,0) - (Math.pow(getMoment(1,0),2)/getMoment(0,0));
    }

    private double zeroTwoNormalInvariant(){
        return getMoment(0,2) - (Math.pow(getMoment(0,1),2)/getMoment(0,0));
    }

    private double zeroZeroNormalInvariant(){
        return getMoment(0,0);
    }

    private double oneOneNormalInvariant(){
        return getMoment(1,1);
    }


    //buczostart
    private double threeZeroGeometricalMoment(){
        return getMoment(3,0)- 3*(getMoment(2,0))*(getMoment(1,0)/getMoment(0,0)) + 2*(getMoment(1,0)*Math.pow((getMoment(1,0)/getMoment(0,0)),2));
    }
    private double zeroThreeGeometricalMoment(){
        return getMoment(0,3)- 3*(getMoment(0,2)*(getMoment(0,1)/getMoment(0,0)) + 2*(getMoment(0,1)*Math.pow((getMoment(0,1)/getMoment(0,0)),2)));
    }
    private double twoOneGeometricalMoment(){
        return getMoment(2,1) - 2*(getMoment(1,1)*(getMoment(1,0)/getMoment(0,0)) - getMoment(2,0)*(getMoment(0,1)/getMoment(0,0)) + 2*(getMoment(0,1)*Math.pow((getMoment(1,0)/getMoment(0,0)),2)));
    }
    private double oneTwoGeometricalMoment(){
        return getMoment(1,2) - 2*(getMoment(1,1)*(getMoment(0,1)/getMoment(0,0)) - getMoment(0,2)*(getMoment(1,0)/getMoment(0,0)) + 2*(getMoment(1,0)*Math.pow((getMoment(0,1)/getMoment(0,0)),2)));
    }

    //buczoend

    public double invariantMomentOne(){
        Double value =(twoZeroNormalInvariant() + zeroTwoNormalInvariant())/(Math.pow(getMoment(0,0),2));
        return round(value,8);
    }

    public double invariantMomentTwo(){
        Double value = (Math.pow(twoZeroNormalInvariant() + zeroTwoNormalInvariant(),2) + 4*Math.pow(oneOneNormalInvariant(),2))/Math.pow(getMoment(0,0),4);
        return round(value,4);
    }
    //buczostart
    public double invariantMomentThree() {
        Double value = (Math.pow(threeZeroGeometricalMoment() + 3 * oneTwoGeometricalMoment(), 2) + Math.pow(3 * twoOneGeometricalMoment() - zeroThreeGeometricalMoment(), 2)) / Math.pow(getMoment(0, 0), 5);
        return round(value, 4);
    }
    public double invariantMomentFour(){
        Double value = (Math.pow(threeZeroGeometricalMoment()+oneTwoGeometricalMoment(),2) + Math.pow(twoOneGeometricalMoment() - zeroThreeGeometricalMoment(),2))/Math.pow(getMoment(0,0),5);
        return round(value,4);
    }
    //buczoend


    public Double round(double value, int number){
        String valueString = value+"";
        number++;
        int size = valueString.length();
        for(int i=0;i<size;i++){
            char single = valueString.charAt(i);
            if(single == '.' && (i+number)<=size){
                return Double.valueOf(valueString.substring(0,i+number));
            }
        }
        return value;
    }

}
