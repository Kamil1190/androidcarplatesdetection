package com.example.kamil.carplatesdetection.models;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kamil.carplatesdetection.R;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-11-29.
 */

public class LearnCharacterAdapter extends BaseAdapter {

    private ArrayList<CharacterValueViewModel> characters;
    private LayoutInflater mInflater;

    public LearnCharacterAdapter(Context context, ArrayList<CharacterValueViewModel> characters) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.characters = characters;
        notifyDataSetChanged();
    }

    public ArrayList<CharacterValueViewModel> getData(){
        return characters;
    }

    @Override
    public int getCount() {
        return characters.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if(convertView == null) {

            convertView = mInflater.inflate(R.layout.learn_row_item, null);
            holder = new ViewHolder(
                    (EditText) convertView.findViewById(R.id.decisionEditText),
                    (TextView) convertView.findViewById(R.id.invariantOneTextView),
                    (TextView) convertView.findViewById(R.id.invariantTwoTextView),
                    (TextView) convertView.findViewById(R.id.invariantThreeTextView),
                    (TextView) convertView.findViewById(R.id.invariantFourTextView),
                    (ImageView) convertView.findViewById(R.id.characterImageView)
            );
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.setValuesAndId(characters.get(position), position);

        holder.decisionEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    final int position = v.getId();
                    final EditText DecisionEditText = (EditText) v;
                    characters.get(position).setDecision(DecisionEditText.getText().toString());
                }
            }
        });

        return convertView;
    }




    class ViewHolder{
        EditText decisionEditText;
        TextView invariantOneTextView;
        TextView invariantTwoTextView;
        TextView invariantThreeTextView;
        TextView invariantFourTextView;
        ImageView characterImageView;

        public ViewHolder(EditText decisionEditText, TextView invariantOneTextView, TextView invariantTwoTextView,TextView invariantThreeTextView,TextView invariantFourTextView, ImageView characterImageView) {
            this.decisionEditText = decisionEditText;
            this.decisionEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            this.invariantOneTextView = invariantOneTextView;
            this.invariantTwoTextView = invariantTwoTextView;
            this.invariantThreeTextView = invariantThreeTextView;
            this.invariantFourTextView = invariantFourTextView;
            this.characterImageView = characterImageView;
        }

        public void setValuesAndId(CharacterValueViewModel valueViewModel, int id){
            decisionEditText.setText(valueViewModel.getDecision());
            decisionEditText.setId(id);
            invariantOneTextView.setText(valueViewModel.getInvariantOne().toString());
            invariantOneTextView.setId(id);
            invariantTwoTextView.setText(valueViewModel.getInvariantTwo().toString());
            invariantTwoTextView.setId(id);
            invariantThreeTextView.setText(valueViewModel.getInvariantThree().toString());
            invariantThreeTextView.setId(id);
            invariantFourTextView.setText(valueViewModel.getInvariantFour().toString());
            invariantFourTextView.setId(id);
            characterImageView.setImageBitmap(valueViewModel.getCharacterBitmap());
            characterImageView.setId(id);
        }

    }
}
