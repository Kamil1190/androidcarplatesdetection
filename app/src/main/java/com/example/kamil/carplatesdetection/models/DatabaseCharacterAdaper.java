package com.example.kamil.carplatesdetection.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.kamil.carplatesdetection.R;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-12-02.
 */

public class DatabaseCharacterAdaper extends ArrayAdapter<Character>{

    public DatabaseCharacterAdaper(Context context, ArrayList<Character> characters) {
        super(context, 0, characters);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result = convertView;

        if(result == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            result = inflater.inflate(R.layout.database_row_item, parent, false);

        }

        Character data = getItem(position);

        final TextView decisionTextView = (TextView) result.findViewById(R.id.decisionTextView);
        final TextView invariantOneTextView = (TextView) result.findViewById(R.id.invariantOneTextView);
        final TextView invariantTwoTextView = (TextView) result.findViewById(R.id.invariantTwoTextView);
        final TextView invariantThreeTextView = (TextView) result.findViewById(R.id.invariantThreeTextView);
        final TextView invariantFourTextView = (TextView) result.findViewById(R.id.invariantFourTextView);

        decisionTextView.setText(data.getDecision());
        invariantOneTextView.setText(data.getInvariantOne()+"");
        invariantTwoTextView.setText(data.getInvariantTwo()+"");
        invariantThreeTextView.setText(data.getInvariantThree()+"");
        invariantFourTextView.setText(data.getInvariantFour()+"");

        return result;
    }
}
