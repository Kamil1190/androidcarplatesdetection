package com.example.kamil.carplatesdetection.imageProcessing;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-11-21.
 */

public class ImageProcessing {


    public Bitmap filterBlackWhite(Bitmap bitmap)
    {
        Bitmap bwBitmap = Bitmap.createBitmap( bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.RGB_565 );
        float[] hsv = new float[ 3 ];
        for( int col = 0; col < bitmap.getWidth(); col++ ) {
            for( int row = 0; row < bitmap.getHeight(); row++ ) {
                Color.colorToHSV( bitmap.getPixel( col, row ), hsv );
                if( hsv[ 2 ] > 0.3f ) {
                    bwBitmap.setPixel( col, row, 0xffffffff );
                } else {
                    bwBitmap.setPixel( col, row, 0xff000000 );
                }
            }
        }

        return bwBitmap;
    }



    public Bitmap getContentPlate(Bitmap bitmap){
        //1 - czarny
        //2 - bialy
        //3 - biało - czarny
        boolean isStart = false;
        boolean[] start = new boolean[3];     //bedzie w tedy jak przejdzie czarny, bialy i napodka bialo-czarny

        boolean isStop = false;
        boolean[] stop = new boolean[2];      //jesli byl juz start i napotka bialy lub czarny

        int yStartContent = 0;
        int yStopContent = 0;

        for(int y=0;y<bitmap.getHeight();y++){
            String colorInLine = getColorInXaxis(y, bitmap);
            if(colorInLine.equals("black") && !isStart){  //napotkano czarny i nie bylo poczatku tablicy
                start[0] = true;
            } else if(colorInLine.equals("white") && !isStart) { //napotkano bialy i nie bylo poczatku tablicy
                start[1] = true;
            } else if(colorInLine.equals("white-black") && !isStart){   //napotkano bialo-czarny i nie bylo poczatku tablicy
                start[2] = true;
            } else if((start[0] == true || start[2] == true )&& start[1] == true && colorInLine.equals("white-black") && !isStart){   //byl (czarny lub bialo-czarny) i bialy a teraz jest bialo-czarny wiec jest to poczatek zawartosci
                start[2] = true;    //bialo - czarny
                isStart = true;     //poczatek zawartosci
                yStartContent = y;  //ustawienie poczatku
                Log.d("color", "start na "+y);
            } else if(isStart && !isStop && (colorInLine.equals("white") || colorInLine.equals("black"))){  //byl poczatek i napotkano na bialy lub czarny
                isStop = true;
                yStopContent = y-6;
                Log.d("color", "stop na "+y);
            }
            Log.d("Color na "+y, colorInLine);
        }

        if(yStartContent > 0 && yStartContent < bitmap.getHeight() && yStopContent > 0 && yStopContent < bitmap.getHeight() ){    //jesli znalezlismy nasze punkty to wycinamy zawartosc
            //Log.d("Values Content", yStartContent+","+yStopContent);
            //Log.d("wycinamy na wysokosc", bitmap.getHeight()-yStopContent+"");
            bitmap = Bitmap.createBitmap(bitmap,0,yStartContent,bitmap.getWidth(),yStopContent);
            return bitmap;
        }
        return null;
    }



    private String getColorInXaxis(int y, Bitmap bitmap){
        boolean[] colors = new boolean[2];    //przechowuje informacje jakie kolory sa w lini poziomej
        colors[0] = false;                    //czarny
        colors[1] = false;                    //bialy

        for(int x=0;x<bitmap.getWidth();x++){   //sprawdzenie jakie kolory znalazly sie w lini poziomej
            int valuePixel = bitmap.getPixel(x,y);

            int red = Color.red(valuePixel);
            int blue = Color.blue(valuePixel);
            int green = Color.green(valuePixel);

            if(red == 255 && blue == 255 && green == 255){
                colors[1] = true;
            } else if(red == 0 && blue == 0 && green == 0){
                colors[0] = true;
            }
        }

        if(colors[1] == true && colors[0] == false){        //zwrocenie wyniku
            return "white";
        } else if(colors[1] == false && colors[0] == true){
            return "black";
        } else {
            return "white-black";
        }
    }

    private String getColorInYaxis(int x, Bitmap bitmap){
        boolean[] colors = new boolean[2];    //przechowuje informacje jakie kolory sa w lini pionowej
        colors[0] = false;                    //czarny
        colors[1] = false;                    //bialy

        for(int y=0;y<bitmap.getHeight();y++){   //sprawdzenie jakie kolory znalazly sie w lini pionowej
            int valuePixel = bitmap.getPixel(x,y);

            int red = Color.red(valuePixel);
            int blue = Color.blue(valuePixel);
            int green = Color.green(valuePixel);
            //int alpha = Color.alpha(valuePixel);
            //Log.d("Color pixel", red+" "+green+" "+blue+" a:"+alpha+" value"+valuePixel);

            if(red == 255 && blue == 255 && green == 255){
                colors[1] = true;
            } else if(red == 0 && blue == 0 && green == 0){
                colors[0] = true;
            }
        }

        if(colors[1] == true && colors[0] == false){        //zwrocenie wyniku
            return "white";
        } else if(colors[1] == false && colors[0] == true){
            return "black";
        } else {
            return "white-black";
        }
    }

    public ArrayList<Bitmap> segmentationCharacters(Bitmap bitmap){

        boolean startCharacter = false;
        int xStartCharacter = -1, xStopCharacter = -1;
        ArrayList<Bitmap> bitmapCharacters = new ArrayList<Bitmap>();

        for(int x=0;x<bitmap.getWidth();x++){
            String colorInVerticalLine = getColorInYaxis(x, bitmap);
            if((colorInVerticalLine.equals("white-black") || colorInVerticalLine.equals("black")) && !startCharacter){
                startCharacter = true;
                xStartCharacter = x;
            } else if(colorInVerticalLine.equals("white") && startCharacter){
                Log.d("find character", xStartCharacter+"-"+xStopCharacter);
                startCharacter = false;
                xStopCharacter = x;

                if(xStartCharacter < xStopCharacter){
                    bitmapCharacters.add(Bitmap.createBitmap(bitmap,xStartCharacter,0,xStopCharacter-xStartCharacter,bitmap.getHeight()));
                }
            }
        }
        return bitmapCharacters;
    }
}
